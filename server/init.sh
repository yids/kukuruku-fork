#!/bin/bash

if [ $# -ne 5 ]; then
  echo "Usage: $0 device rate ppm fft-size listen-address"
  exit 2
fi

sdr="/tmp/sdrpipe"
tune="/tmp/tunepipe"
gain="30"
ppm="$3"
sample_rate="$2"
listen_address="$5"
freq="$(( 100 * 1000 * 1000 ))"
fft_size="$4"

[ ! -p "$sdr" ] && mkfifo "$sdr"
[ ! -p "$tune" ] && mkfifo "$tune"

# set -b :: for ipv4 and ipv6 all interfaces
./server -p "$ppm" -f "$freq" -i "$tune" -o "$sdr" -f "$freq" -g "$gain" -a -s "$sample_rate" -b "$listen_address" -t 4444 -w "$fft_size" &
spid=$!

echo "Server PID $spid, use 'gdb ./server $spid -ex c' to debug"

trap "kill $spid" SIGINT SIGTERM

./osmosdr-input.py -d "$1" -r "$2" -i "$tune" -o "$sdr" -f "$freq" -g "$gain" -p "$ppm"
kill $spid
